#include <arpa/inet.h>
#include <unistd.h> 
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string.h>

#define LOKAL_PORT 12345
#define BREV_STR 50
#define BAK_LOGG 10 // Størrelse på for kø ventende forespørsler 

		int sd, ny_sd;

		typedef struct sesjon {
		char buf[2]; //Oppretter buffer for å ta imot heltall
		void (*spill)(int, int); // Peker til void-funksjon med to heltallsargumenter
} sesjon_t;

void grat(int g, int t)
{ 
		write(ny_sd,"Gratulerer!\n", 13); //Skriver tilbake til klient
		printf("Vinnertallet er: %d\nSpiller: %d valgte: %d og vant!\n\n",t,ny_sd,g); //Gir feedback i tjener vinduet

} // Gratulerer

void kond(int g, int t)
{ 
		write(ny_sd, "Beklager :( \n", 13); //Skriver tilbake til klient
		printf("Vinnertallet er: %d\nSpiller: %d valgte: %d og tapte!\n\n",t,ny_sd,g); //Gir feedback i tjener vinduet

} // Kondolerer

void spill(int g, int t)
{ 
		if(g == t) grat(g, t); //Spiller vinner 
		else kond(g, t);//Spiller taper
}
int main ()
{
		
		struct sockaddr_in  lok_adr; // Lokal adresse
		struct sockaddr_in  fj_adr;  // Fjern adresse

		char brev_buffer[BREV_STR]; //Oppretter en buffer for å håndtere read (input) fra klienter
		socklen_t adr_len;
		int brv_len;



		// Setter opp socket-strukturen
		sd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);


		// Initierer lokal adresse
		lok_adr.sin_family = AF_INET;
		lok_adr.sin_port = htons((u_short)LOKAL_PORT); 
		lok_adr.sin_addr.s_addr = htonl(INADDR_ANY);

		// Kobler sammen socket og lokal adresse
		bind(sd, (struct sockaddr *)&lok_adr, sizeof(lok_adr)); 

		// Venter på forespørsel om forbindelse
		listen(sd, BAK_LOGG); 
		while(1){ 

				// Aksepterer mottatt forespørsel
				//Oprpetter en ny socket som inneholder informasjon som legges inn i brev_buffer
				ny_sd = accept(sd, (struct sockaddr *)&fj_adr, &adr_len);
				fprintf(stdout, "User %d joined the game!\n\n",ny_sd);

				//Hver gang en klient forespør og tjeneren aksepterer, så oppretter den en egen process for denne klienten
				if(0==fork()){
						while (1){

								// Leser fra socket via fil-deskriptor og finner lengden på stringen
								brv_len = read(ny_sd, brev_buffer, BREV_STR);

								// Avslutter dersom det er slutt på datastrømmen
								//Lengden på stringen brukes her til å skjekke om det er en klient tilkoblet.
								//Hvis arrayen brev_buffer er tom, betyr det at klienten har forlatt spillet.
								if (brv_len <= 0)
								{
										printf("User %d left\n\n", ny_sd);
										return 0;
								}

								sesjon_t s; //Setter s som variabel når det refereres til structen
								srand(time(NULL)); // Initierer slumptallsgenerator
								sleep(1); //Programmet venter i et sekund slik at man ikke kan vinne mange ganger på samme vinnertall.
								s.spill=spill; //structen sitt spill refereres til spill funksjonen i denne koden.
								//scanf("%s", s.buf);
								// Kjorer spillet
								s.spill( atoi(brev_buffer), // Oversetter tekststreng til heltall
												rand()%6+1    // Kaster en 6-sidet terning
									   );

						}
				}
		}
		return 0;

}
