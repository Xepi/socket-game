#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#define FJERN_PORT 12345 

int main (void)
{
		int sd;
		struct sockaddr_in  fj_adr;  // Fjern adresse

		//Setter opp socket-strukturen
		sd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

		// Initierer fjern adresse
		fj_adr.sin_family = AF_INET;
		fj_adr.sin_port = htons((u_short)FJERN_PORT); 
		inet_pton (AF_INET, "127.0.0.1", &(fj_adr.sin_addr.s_addr));

		// Oppretter forbindelse
		connect(sd, (struct sockaddr *)&fj_adr, sizeof(fj_adr));
		
		char brev[100] = "Her kommer et brev";
		
		while(1)
		{// Skriver til socket via fil-deskriptor
		fgets(brev, 100, stdin); 
		write(sd, brev, strlen(brev));
		//write(sd, "\n", 1);
		}
		return 0;
}
