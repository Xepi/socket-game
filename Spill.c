#include <stdio.h>
#include <stdlib.h>
#include <time.h>
/* Versjon 2 av 'Det sårbare spillet'
av Thomas Nordli 6/11-2014
*/
typedef struct sesjon //Setter opp en struct
{
		char buf[2]; //Oppretter en buffer
		void (*spill)(int, int); // Peker til void-funksjon med to heltallsargumenter
} sesjon_t;

void grat(){ printf("Gratulerer\n"); exit(0); } // Gratulerer
void kond(){ printf("Kondolerer\n"); exit(1); } // Kondolerer
void spill(int g, int t){ if(g == t) grat(); else kond(); } //Skjekker om heltallene er like!

int main()
{
		sesjon_t s;
		srand(time(NULL)); // Initierer slumptallsgenerator
		s.spill=spill;
		scanf("%s", s.buf); //Leser inn en string
		printf("Tall: %s Adresse: %p\n",s.buf,s.buf);
		// Kjorer spillet
		s.spill( atoi(s.buf), // Oversetter tekststreng til heltall
		rand()%6+1 // Kaster en 6-sidet terning
		);
		return 2; // Skal aldri komme hit
}
